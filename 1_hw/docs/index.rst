.. controls_Ermakov documentation master file, created by
   sphinx-quickstart on Fri Sep 29 17:02:16 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to controls_Ermakov's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Documentation myproject
======================

.. toctree::
   :maxdepth: 2
   :caption: example of automatic-generative docs views

   ./views.rst