# Test package

## Sequence of commands for creating a virtual environment:
```
>> mkdir YOUR_PACKAGE_NAME
>> cd YOUR_PACKAGE_NAME
>> touch __init__.py
>> cd ..
>> touch pyproject.toml
>> open pyproject.toml
...
>> touch LICENSE
>> open LICENSE
...
>> touch README.md
>> open README.md
...
>> python3 -m build
```

## Creating documentation using sphinx:
```
>> mkdir docs
>> cd docs
>> sphinx-quickstart
>> ...
>> touch views.py
>> open views.py
>> ...
>> cd source
>> open index.rst
>> ...
```